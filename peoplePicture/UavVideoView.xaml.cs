﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WebEye.Controls;
using System.Drawing;
using System.IO;
using System.Threading;
using System.ComponentModel;
using System.Data;
using System.Net.Http;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using Newtonsoft.Json;
using VideoSteamFaceRecognition;

namespace peoplePicture
{
    /// <summary>
    /// UavVideoView.xaml 的交互逻辑
    /// </summary>
    public partial class UavVideoView
    {

        private Thread _thread;

        public UavVideoView()
        {
            InitializeComponent();
        }

        private void HandlePlayButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var uri = new Uri(_urlTextBox.Text);
                _streamPlayerControl.StartPlay(uri);
                _statusLabel.Text = "Connecting...";
            }
            catch (Exception ex)
            {
                //SystemLog.Log(ex);
            }

        }

        public void Play()
        {
            this.HandlePlayButtonClick(null, null);
        }

        private void HandleStopButtonClick(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        public void Stop()
        {
            try
            {
                if (_streamPlayerControl.IsPlaying)
                    _streamPlayerControl.Stop();
            }
            catch (Exception ex)
            {
                //SystemLog.Log(ex);
            }

        }

        private void HandleImageButtonClick(object sender, RoutedEventArgs e)
        {
            //var dialog = new SaveFileDialog { Filter = "Bitmap Image|*.bmp" };
            //if (dialog.ShowDialog() == true)
            //{
            //    _streamPlayerControl.GetCurrentFrame().Save(dialog.FileName);
            //}
        }

        private void UpdateButtons()
        {
            _playButton.IsEnabled = !_streamPlayerControl.IsPlaying;
            _stopButton.IsEnabled = _streamPlayerControl.IsPlaying;
            _imageButton.IsEnabled = _streamPlayerControl.IsPlaying;
        }

        private void HandlePlayerEvent(object sender, RoutedEventArgs e)
        {
            UpdateButtons();
            Console.WriteLine("UpdateButtons ");
            if (e.RoutedEvent.Name == "StreamStarted")
            {
                _statusLabel.Text = "Playing";
                Console.WriteLine("Playing ");
                getVideoFrame();
            }
            else if (e.RoutedEvent.Name == "StreamFailed")
            {
                _statusLabel.Text = "Failed";
                if (_thread != null)
                    _thread.Abort();
                MessageBox.Show(
                    ((WebEye.Controls.Wpf.StreamPlayerControl.StreamFailedEventArgs)e).Error,
                    "Stream Player",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            else if (e.RoutedEvent.Name == "StreamStopped")
            {
                _statusLabel.Text = "Stopped";
            }
        }

        private void getVideoFrame()
        {
            _thread = new Thread(() =>
            {
                var timeSpan = 500;
                while (true)
                {
                    Thread.Sleep(timeSpan);

                    this.Dispatcher.Invoke(() =>
                    {
                        try
                        {
                            Bitmap image1;
                            image1 = _streamPlayerControl.GetCurrentFrame();


                            BitmapSource source =
                                        System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(image1.GetHbitmap(),
                                        IntPtr.Zero,
                                        Int32Rect.Empty,
                                        BitmapSizeOptions.FromEmptyOptions());
                            _streamImage.Width = image1.Width;
                            _streamImage.Height = image1.Height;
                            _streamImage.Source = source;
                            Console.WriteLine("image1 : " + image1.Width +  "   " + image1.Height);

                            var faceFeature = BeginDetection(image1);
                            if (faceFeature.error_code == "0")
                            {
                                int facecount = faceFeature.result.face_list.Count();

                                for (int i = 0; i < facecount; i++)
                                {
                                    Console.WriteLine("face count : " + facecount);

                                    System.Windows.Shapes.Rectangle myRect = new System.Windows.Shapes.Rectangle();
                                    myRect.Stroke = System.Windows.Media.Brushes.Black;
                                    //myRect.Fill = System.Windows.Media.Brushes.SkyBlue;
                                    myRect.HorizontalAlignment = HorizontalAlignment.Left;
                                    myRect.VerticalAlignment = VerticalAlignment.Center;
                                    myRect.Height = faceFeature.result.face_list[i].location.height;//x2
                                    myRect.Width = faceFeature.result.face_list[i].location.width;//y2
                                    Canvas.SetTop(myRect, faceFeature.result.face_list[i].location.left);// x1
                                    Canvas.SetLeft(myRect, faceFeature.result.face_list[i].location.top);//y1
                                    _canvas.Children.Add(myRect);
                                }
                            }

                        }
                        catch
                        {
                            _thread.Abort();
                        }

                    });
                }
            });

            _thread.IsBackground = true;
            _thread.Start();
        }

        private FaceFeatures BeginDetection(Bitmap image)
        {
            MemoryStream ms = new MemoryStream();
            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] arr = new byte[ms.Length];
            ms.Position = 0;
            ms.Read(arr, 0, (int)ms.Length);
            ms.Close();
            String strbaser64 = Convert.ToBase64String(arr);

            //显示数据
            FaceFeatures faceFeature = FaceRecognition.Detect(strbaser64);

            return faceFeature;
        }


        private void UIElement_OnPreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            try { base.DragMove(); } catch (Exception) { }
        }

        private void windowLoaded(object sender, RoutedEventArgs e)
        {
            this.Play();
            var parent = Window.GetWindow(this);
            if (parent != null)
                parent.Closed += (sender2, e2) => { Stop(); };
        }
    }
}
